const number = 1; // Primitive Type (num, string, booleans)
const num2 = number;

const person = { //Stored in memory
  name: 'Zach';
};

const secondPerson = person; //Not stored in memory, only a pointer

person.name = 'Manu'; //Modifies memory for both first and second person

const thirdPerson = { //Adds this person to memory
  ...person
};

console.log(secondPerson);
