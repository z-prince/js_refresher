// The spread and rest operators are three dots(...)

// Spread is used to spit up array elements or object properties

const numbers = [1, 2, 3];
const newNumbers = [...numbers, 4]

console.log(newNumbers);

const person = {
  name: 'Max'
};

const newPerson = {
  ...person,
  age: 28
}

console.log(newPerson);
// Rest operator is used to merge a list of function arguments into an array

const filter = (...args) => {
  return args.filter(el => el === 1);
}

console.log(filter(1, 2, 3));
