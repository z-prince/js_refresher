const numbers = [1, 2, 3];

const doubleArray = numbers.map((num) => { //will create a new array in memory
  return num * 2;
});


console.log(numbers);
console.log(doubleArray);
